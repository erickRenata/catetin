package com.catet.`in`.utils.date

import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by @erickrenata on 11/05/20.
 */

class DateUtils {
    companion object {

        fun calculateRemainingDay(date: String): String {
            var result = ""
            var remainingDay = 0
            val calCurr = Calendar.getInstance()
            val day = Calendar.getInstance()
            day.time = SimpleDateFormat("yyyy-MM-dd").parse(date)
            if (day.after(calCurr)) {
                remainingDay = day[Calendar.DAY_OF_MONTH] - calCurr[Calendar.DAY_OF_MONTH]
            }

            result = if (remainingDay in 1..3) {
                "$remainingDay hari lagi"
            } else if (remainingDay == 0) {
                "Hari ini"
            } else {
                toSimpleDateFormat(date)
            }
            return result
        }

        fun toSimpleDateFormat(dateTime: String): String {
            var result = ""
            try {
                val fmt = SimpleDateFormat("yyyy-MM-dd")
                val date = fmt.parse(dateTime)
                val fmtOut =
                    SimpleDateFormat("dd MMMM yyyy")
                result = fmtOut.format(date)
                return result
            } catch (e: ParseException) {
                e.printStackTrace()
            }
            return result!!
        }

        fun parseDateToAPI(dateTime: String): String {
            var result = ""
            try {
                val fmt = SimpleDateFormat("dd MMMM yyyy")
                val date = fmt.parse(dateTime)
                val fmtOut =
                    SimpleDateFormat("dd-MM-yyyy")
                result = fmtOut.format(date)
                return result
            } catch (e: ParseException) {
                e.printStackTrace()
            }
            return result!!
        }

        fun parseDateFromResponse(dateTime: String): String {
            var result = ""
            try {
                val fmt = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
                val date = fmt.parse(dateTime)
                val fmtOut =
                    SimpleDateFormat("dd MMMM yyyy")
                result = fmtOut.format(date)
                return result
            } catch (e: ParseException) {
                e.printStackTrace()
            }
            return result!!
        }

        fun toDateFormat(dateTime: String): Date {
            var date = Date()
            try {
                if (dateTime.isEmpty()) {
                    return date
                }
                val fmt = SimpleDateFormat("dd MMMM yyyy")
                date = fmt.parse(dateTime)
                return date
            } catch (e: ParseException) {
                e.printStackTrace()
            }
            return date
        }
    }
}