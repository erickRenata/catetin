package com.catet.`in`.utils.animation

import android.view.View
import com.daimajia.androidanimations.library.Techniques
import com.daimajia.androidanimations.library.YoYo

/**
 * Created by @erickrenata on 11/05/20.
 */

class YoyoAnimator {

    companion object {

        fun fadeInAnimation(view: View, duration: Long) {
            YoYo.with(Techniques.FadeIn)
                .duration(duration)
                .playOn(view)
        }

        fun fadeOutAnimation(view: View, duration: Long) {
            YoYo.with(Techniques.FadeOut)
                .duration(duration)
                .playOn(view)
        }

        fun slideInDownAnimation(view: View, duration: Long) {
            YoYo.with(Techniques.SlideInDown)
                .duration(duration)
                .playOn(view)
        }

        fun slideOutUpAnimation(view: View, duration: Long) {
            YoYo.with(Techniques.SlideOutUp)
                .duration(duration)
                .playOn(view)
        }

        fun bounceInAnimation(view: View, duration: Long) {
            YoYo.with(Techniques.BounceIn)
                .duration(duration)
                .playOn(view)
        }
    }
}