package com.catet.`in`.utils.date

import android.app.DatePickerDialog
import android.content.Context
import android.widget.DatePicker


/**
 * Created by @erickrenata on 24/05/20.
 */
class MyDatePickerDialog(
    context: Context,
    callBack: OnDateSetListener?,
    year: Int,
    monthOfYear: Int,
    dayOfMonth: Int
) :
    DatePickerDialog(context, callBack, year, monthOfYear, dayOfMonth) {

    private var title: String? = null

    fun setPermanentTitle(title: String?) {
        this.title = title
        setTitle(title)
    }

    override fun onDateChanged(view: DatePicker, year: Int, month: Int, dayOfMonth: Int) {
        super.onDateChanged(view, year, month, dayOfMonth)
        setTitle(title)
    }
}