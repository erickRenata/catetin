package com.catet.`in`.utils

import android.content.Context
import android.content.pm.PackageManager
import android.net.Uri
import android.provider.MediaStore
import android.provider.Settings
import com.orhanobut.hawk.Hawk
import java.text.DecimalFormat
import java.text.DecimalFormatSymbols
import java.util.*


/**
 * Created by @erickrenata on 11/05/20.
 */

class Utils {

    companion object {

        fun getVersionApp(mContext: Context): String {
            val manager = mContext.packageManager
            val info = manager.getPackageInfo(mContext.packageName, PackageManager.GET_ACTIVITIES)
            return info.versionName
        }

        fun getFirebaseTokenId(): String? {
            return Hawk.get<String>("token") ?: return ""
        }

        fun getDeviceId(context: Context): String? {
            return Settings.Secure.getString(
                context.contentResolver,
                Settings.Secure.ANDROID_ID
            )
        }

        fun getRealPathFromURI(
            contentURI: Uri,
            context: Context
        ): String? {
            val result: String?
            val cursor =
                context.contentResolver.query(contentURI, null, null, null, null)
            if (cursor == null) {
                result = contentURI.path
            } else {
                cursor.moveToFirst()
                val idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA)
                result = cursor.getString(idx)
                cursor.close()
            }
            return result
        }


        fun convertNumberWithoutCurrency(number: Int): String? {
            val kursIndonesia =
                DecimalFormat.getCurrencyInstance() as DecimalFormat
            val formatRp = DecimalFormatSymbols()
            formatRp.currencySymbol = ""
            formatRp.monetaryDecimalSeparator = ','
            formatRp.groupingSeparator = '.'
            kursIndonesia.decimalFormatSymbols = formatRp
            val result: String
            val recentLanguage = Locale.getDefault().displayLanguage
            result = if (recentLanguage == "Bahasa Indonesia") {
                kursIndonesia.format(number.toLong())
            } else if (recentLanguage == "Indonesia") {
                kursIndonesia.format(number.toLong())
            } else {
                kursIndonesia.format(number.toLong())
                    .substring(0, kursIndonesia.format(number.toLong()).length - 3)
            }
            return result
        }

        fun convertNumberWithRp(number: Int): String? {
            val kursIndonesia =
                DecimalFormat.getCurrencyInstance() as DecimalFormat
            val formatRp = DecimalFormatSymbols()
            formatRp.currencySymbol = "Rp "
            formatRp.monetaryDecimalSeparator = '.'
            formatRp.groupingSeparator = '.'
            kursIndonesia.decimalFormatSymbols = formatRp
            val result: String
            val recentLanguage = Locale.getDefault().displayLanguage
            result = if (recentLanguage == "Bahasa Indonesia") {
                kursIndonesia.format(number.toLong())
            } else if (recentLanguage == "Indonesia") {
                kursIndonesia.format(number.toLong())
            } else {
                kursIndonesia.format(number.toLong())
                    .substring(0, kursIndonesia.format(number.toLong()).length - 3)
            }
            return result.replace(",",".")
        }

    }
}