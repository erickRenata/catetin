package com.catet.`in`.network

import com.catet.`in`.base.BaseResponse
import com.catet.`in`.model.response.ResponseList
import com.catet.`in`.model.response.ResponseObject
import com.catet.`in`.model.response.dashboard.Dashboard
import com.catet.`in`.model.response.hutang.HutangData
import com.catet.`in`.model.response.login.Login
import com.catet.`in`.model.response.piutang.PiutangData
import com.catet.`in`.model.response.profile.Profile
import io.reactivex.Observable
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.http.*


/**
 * Created by @erickrenata on 11/05/20.
 */

interface ApiService {

    /***** Auth *****/
    @FormUrlEncoded
    @POST("oauth/token")
    fun postLogin(
        @Field("grant_type") grantType: String,
        @Field("client_id") clientId: Int,
        @Field("client_secret") clientSecret: String,
        @Field("username") username: String,
        @Field("password") password: String
    ): Observable<Login>

    @FormUrlEncoded
    @POST("api/social")
    fun postLoginMedsos(
        @Field("grant_type") grantType: String,
        @Field("client_id") clientId: Int,
        @Field("client_secret") clientSecret: String,
        @Field("media_token") mediaToken: String,
        @Field("media_type") mediaType: String,
        @Field("device_id") deviceId: String
    ): Observable<Login>

    @GET("api/profile")
    fun getUserProfile(): Observable<ResponseObject<Profile>>

    /***** Dashboard *****/
    @GET("api/dashboard")
    fun getDetailDashboard(): Observable<ResponseObject<Dashboard>>

    /***** Hutang *****/
    @GET("api/hutang")
    fun getHutangList(): Observable<ResponseList<HutangData>>

    @GET("api/hutang/detail/{id}")
    fun getHutangDetail(@Path("id") id: String): Observable<ResponseObject<HutangData>>

    @FormUrlEncoded
    @POST("api/hutang/save")
    fun postCatatHutang(
        @Field("email") email: String,
        @Field("date") date: String,
        @Field("due_date") dueDate: String,
        @Field("description") description: String,
        @Field("location") location: String,
        @Field("value") value: String
    ): Observable<BaseResponse>

    @Multipart
    @POST("api/hutang/save")
    fun postUpdateCatatHutang(
        @Part("email") email: RequestBody,
        @Part("date") date: RequestBody,
        @Part("due_date") due_date: RequestBody,
        @Part("description") description: RequestBody,
        @Part("location") location: RequestBody,
        @Part("value") value: RequestBody,
        @Part bukti_bayar: MultipartBody.Part,
        @Part("id") id: RequestBody
    ): Observable<BaseResponse>

    @Multipart
    @POST("api/hutang/save")
    fun postUpdateCatatHutang(
        @Part("email") email: RequestBody,
        @Part("date") date: RequestBody,
        @Part("due_date") due_date: RequestBody,
        @Part("description") description: RequestBody,
        @Part("location") location: RequestBody,
        @Part("value") value: RequestBody,
        @Part bukti_bayar: MultipartBody.Part
    ): Observable<BaseResponse>

    /***** Piutang *****/
    @GET("api/piutang")
    fun getPiutangList(): Observable<ResponseList<PiutangData>>

    @GET("api/piutang/detail/{id}")
    fun getPiutangDetail(@Path("id") id: String): Observable<ResponseObject<PiutangData>>

    @FormUrlEncoded
    @POST("api/piutang/save")
    fun postCatatPiutang(
        @Field("email") email: String,
        @Field("date") date: String,
        @Field("due_date") dueDate: String,
        @Field("description") description: String,
        @Field("location") location: String,
        @Field("value") value: String
    ): Observable<BaseResponse>

    @Multipart
    @POST("api/piutang/save")
    fun postUpdateCatatPiutang(
        @Part("email") email: RequestBody,
        @Part("date") date: RequestBody,
        @Part("due_date") due_date: RequestBody,
        @Part("description") description: RequestBody,
        @Part("location") location: RequestBody,
        @Part("value") value: RequestBody,
        @Part bukti_transfer: MultipartBody.Part,
        @Part("id") id: RequestBody
    ): Observable<BaseResponse>

    @Multipart
    @POST("api/piutang/save")
    fun postUpdateCatatPiutang(
        @Part("email") email: RequestBody,
        @Part("date") date: RequestBody,
        @Part("due_date") due_date: RequestBody,
        @Part("description") description: RequestBody,
        @Part("location") location: RequestBody,
        @Part("value") value: RequestBody,
        @Part bukti_transfer: MultipartBody.Part
    ): Observable<BaseResponse>

    /***** Change Status *****/
    @FormUrlEncoded
    @POST("api/hutang/change/status")
    fun postChangeStatus(
        @Field("id") id: String,
        @Field("status") status: String
    ): Observable<BaseResponse>

    /***** Notify *****/
    @GET("api/notify/hutang/{id}")
    fun getNotifyPayDebt(@Path("id") id: String): Observable<BaseResponse>

    @GET("api/notify/status/{id}")
    fun getNotifyDebtPaid(@Path("id") id: String): Observable<BaseResponse>
}