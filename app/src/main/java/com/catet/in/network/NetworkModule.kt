package com.catet.`in`.network

import android.util.Log
import com.catet.`in`.constant.Constant
import com.catet.`in`.storage.Preferences
import okhttp3.Interceptor
import okhttp3.JavaNetCookieJar
import okhttp3.OkHttpClient
import okhttp3.Response
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.io.IOException
import java.net.CookieManager
import java.net.CookiePolicy
import java.util.concurrent.TimeUnit


/**
 * Created by @erickrenata on 11/05/20.
 */

object NetworkModule {

    fun create(): ApiService {
        val requestInterface = Retrofit.Builder()
            .client(provideOkHttpClient())
            .baseUrl(Constant.BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .build()
        return requestInterface.create(ApiService::class.java)
    }

    private fun provideOkHttpClient(): OkHttpClient? {
        val cookieManager = CookieManager()
        cookieManager.setCookiePolicy(CookiePolicy.ACCEPT_ALL)
        val interceptor =
            HttpLoggingInterceptor(HttpLoggingInterceptor.Logger { message: String? ->
                Log.d(
                    "SolidApp",
                    message
                )
            })
        interceptor.level = HttpLoggingInterceptor.Level.BODY
        return OkHttpClient.Builder().connectTimeout(40, TimeUnit.SECONDS)
            .readTimeout(40, TimeUnit.SECONDS)
            .cookieJar(JavaNetCookieJar(cookieManager)).addInterceptor(interceptor)
            .addNetworkInterceptor(
                object : Interceptor {
                    @Throws(IOException::class)
                    override fun intercept(chain: Interceptor.Chain): Response {
                        val request = chain.request()
                        var token = ""
                        if (Preferences.getLogin() != null) {
                            token = Preferences.getLogin()!!.access_token
                        }
                        val newRequest = request.newBuilder()
                            .addHeader("Accept", "application/json")
                            .addHeader("Content-Type", "application/json")
                            .addHeader("Authorization", "Bearer $token")
                            .addHeader("Cache-Control", "no-cache")
                            .addHeader("Cache-Control", "no-store")
                            .build()
                        return chain.proceed(newRequest)
                    }
                }
            )
            .build()
    }
}