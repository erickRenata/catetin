package com.catet.`in`.network

import android.content.Context
import android.content.Intent
import android.widget.Toast
import com.catet.`in`.model.error.ErrorModel
import com.catet.`in`.model.error.Errors
import com.google.gson.Gson
import retrofit2.adapter.rxjava2.HttpException


/**
 * Created by @erickrenata on 11/05/20.
 */

class ErrorHandler {

    companion object {
        fun handleErrorAPI(throwable: Throwable, context: Context) {
            if (throwable is HttpException) {
                val exception: HttpException = throwable
                when (exception.code()) {
                    401 -> {
                        val res =
                            throwable.response().errorBody()!!.string()
                        val gson = Gson()
                        val baseResponse: Errors =
                            gson.fromJson(res, Errors::class.java)

                        if (baseResponse.message == "Unauthorized") {
//                            Preferences.removeProfileResponse()
//                            Preferences.removeLoginResponse()
//                            context.startActivity(
//                                Intent(context, SplashAct::class.java)
//                                    .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
//                            )
                        } else {
                            Toast.makeText(context, baseResponse.message, Toast.LENGTH_SHORT)
                                .show()
                        }
                    }
                    422 -> {
                        val res =
                            throwable.response().errorBody()!!.string()
                        val gson = Gson()
                        val baseResponse: ErrorModel =
                            gson.fromJson(res, ErrorModel::class.java)
                        Toast.makeText(context, baseResponse.meta.message, Toast.LENGTH_SHORT)
                            .show()
                    }
                    500 -> {
                        Toast.makeText(context, "Something Error", Toast.LENGTH_SHORT).show()
                    }
                    else -> {
                        Toast.makeText(context, "Something Error", Toast.LENGTH_SHORT).show()
                    }
                }
            }
        }
    }
}