package com.catet.`in`

import android.app.Application
import android.content.Context
import android.util.Log
import com.orhanobut.hawk.Hawk
import io.github.inflationx.calligraphy3.CalligraphyConfig
import io.github.inflationx.calligraphy3.CalligraphyInterceptor
import io.github.inflationx.viewpump.ViewPump

/**
 * Created by @erickrenata on 10/05/20.
 */

open class CatetinApp : Application() {

    init {
        instance = this
    }

    companion object {
        private var instance: CatetinApp? = null

        fun applicationContext(): Context {
            return instance!!.applicationContext
        }
    }

    override fun onCreate() {
        super.onCreate()

        setupHawk()
        setupCalligraphy()
        applicationContext()
    }

    private fun setupCalligraphy() {
        ViewPump.init(
            ViewPump.builder()
                .addInterceptor(
                    CalligraphyInterceptor(
                        CalligraphyConfig.Builder()
                            .setDefaultFontPath("fonts/Gotham_Book.otf")
                            .build()
                    )
                )
                .build()
        )
    }


    private fun setupHawk() {
        Hawk.init(this)
            .build()
    }
}