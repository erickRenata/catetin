package com.catet.`in`.listener

/**
 * Created by @erickrenata on 11/05/20.
 */

interface OnItemClickListener {
    fun onClickItem(position: Int)
}