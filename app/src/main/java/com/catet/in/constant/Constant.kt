package com.catet.`in`.constant


/**
 * Created by @erickrenata on 11/05/20.
 */

class Constant {
    companion object {
        /* Staging */
        const val BASE_URL = "https://catetin.kolaborasi.co/"

        /* Passing Parameter API */
        const val GRANT_TYPE = "password"
        const val CLIENT_ID = 4
        const val CLIENT_SECRET = "V1629g8zd5HWTpTAg22I8xJKOwXBvQfhtulWnfqD"

        /* Topic Firebase */
        const val FCM_TOPIC_GENERAL_DEV = "dev-general-notif"

        /* Passing Parameter Intent */
        const val INTENT_NOTIFICATION_ID = "idFromNotif"
        const val INTENT_NOTIFICATION_TYPE = "typeFromNotif"
        const val RESULT_INTENT_ADD_CARD_CODE = 100

        /* Cons */
        const val LUNAS = "LUNAS"
        const val BELUM_LUNAS = "BELUM LUNAS"
        const val HUTANG = "HUTANG"
        const val PIUTANG = "PIUTANG"

        const val PICK_FROM_CAMERA = 111
        const val PICK_FROM_GALLERY = 222

    }
}