package com.catet.`in`.fcm

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.media.RingtoneManager
import android.os.Build
import androidx.core.app.NotificationCompat
import com.catet.`in`.R
import com.catet.`in`.constant.Constant.Companion.INTENT_NOTIFICATION_ID
import com.catet.`in`.constant.Constant.Companion.INTENT_NOTIFICATION_TYPE
import com.catet.`in`.storage.Preferences
import com.catet.`in`.ui.dashboard.DashboardAct
import com.catet.`in`.ui.splash.SplashAct
import com.google.firebase.iid.FirebaseInstanceId
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.orhanobut.hawk.Hawk


/**
 * Created by @erickrenata on 11/05/20.
 */

class MyFirebaseMessagingService : FirebaseMessagingService() {

    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        super.onMessageReceived(remoteMessage)
        val data = remoteMessage.data
        val id = data["idhutang"].toString()
        val type = data["type"].toString()
        showNotification(
            remoteMessage.notification!!.title,
            remoteMessage.notification!!.body,
            id,
            type
        )
    }

    override fun onNewToken(s: String) {
        super.onNewToken(s)
        Hawk.put("tokenfcm", s)
    }

    fun showNotification(
        title: String?,
        body: String?,
        id: String,
        type: String
    ) {
        val intent: Intent
        if (Preferences.getProfile() == null) {
            intent = Intent(this, SplashAct::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        } else {
            intent = Intent(this, DashboardAct::class.java)
                .putExtra(INTENT_NOTIFICATION_ID, id)
                .putExtra(INTENT_NOTIFICATION_TYPE, type)
                .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
        }


        val pendingIntent = PendingIntent.getActivity(
            this, 0 /* Request code */, intent,
            PendingIntent.FLAG_ONE_SHOT
        )
        val channelId = getString(R.string.default_notification_channel_id)
        val defaultSoundUri =
            RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
        val notificationBuilder =
            NotificationCompat.Builder(this, channelId)
                .setSmallIcon(R.mipmap.ic_launcher) //                        .setContentTitle(getString(R.string.app_name))
                .setContentTitle(title)
                .setContentText(body)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent)
        val notificationManager =
            getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        // Since android Oreo notification channel is needed.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channel = NotificationChannel(
                channelId,
                "Channel human readable title",
                NotificationManager.IMPORTANCE_DEFAULT
            )
            notificationManager.createNotificationChannel(channel)
        }
        notificationManager.notify(0 /* ID of notification */, notificationBuilder.build())
    }

    companion object {
        val token: String
            get() = FirebaseInstanceId.getInstance().token!!
    }
}
