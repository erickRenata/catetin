package com.catet.`in`.ui.login

import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.util.Base64
import android.util.Log
import com.catet.`in`.R
import com.catet.`in`.base.BaseActivity
import com.catet.`in`.constant.Constant.Companion.CLIENT_ID
import com.catet.`in`.constant.Constant.Companion.CLIENT_SECRET
import com.catet.`in`.constant.Constant.Companion.GRANT_TYPE
import com.catet.`in`.fcm.MyFirebaseMessagingService
import com.catet.`in`.model.response.profile.Profile
import com.catet.`in`.network.ErrorHandler
import com.catet.`in`.network.NetworkModule
import com.catet.`in`.storage.Preferences
import com.catet.`in`.ui.dashboard.DashboardAct
import com.facebook.*
import com.facebook.login.LoginManager
import com.facebook.login.LoginResult
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_login.*
import org.json.JSONException
import org.json.JSONObject
import java.security.MessageDigest
import java.security.NoSuchAlgorithmException

/**
 * Created by @erickrenata on 13/05/20.
 */

class LoginAct : BaseActivity() {

    private lateinit var tokenFB: String

    private lateinit var imageUrl: String
    private lateinit var name: String
    private lateinit var email: String

    private var googleSignInClient: GoogleSignInClient? = null
    var callbackManager: CallbackManager? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        initOnClickListener()
        initFacebook()
        initLoginGoogle()
        initKeyhash()
    }

    private fun initOnClickListener() {
        btn_login_google.setOnClickListener {
            val signInIntent: Intent = googleSignInClient!!.signInIntent
            startActivityForResult(signInIntent, 101)
        }
        btn_login_facebook.setOnClickListener {
            login_button_fb.performClick()
        }
    }

    /***** Login Google */
    private fun initLoginGoogle() {
        val gso =
            GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.client_id))
                .requestEmail()
                .build()
        googleSignInClient = GoogleSignIn.getClient(this, gso)
    }

    /***** Login Facebook */
    private fun initFacebook() {
        FacebookSdk.sdkInitialize(applicationContext)
        callbackManager = CallbackManager.Factory.create()
        login_button_fb.setReadPermissions(
            listOf(
                "public_profile", "email"
            )
        )
        login_button_fb.registerCallback(callbackManager, object : FacebookCallback<LoginResult> {
            override fun onSuccess(loginResult: LoginResult) {
                // App code
                val request = GraphRequest.newMeRequest(
                    loginResult.accessToken
                ) { `object`: JSONObject, response: GraphResponse ->
                    Log.v("LoginActivity", response.toString())

                    // Application code
                    try {
                        val userId = `object`.getString("id")
                        email = `object`.getString("email")
                        name = `object`.getString("name")
                        //                                String birthday = object.getString("birthday");
                        //                                String lastName = object.getString("last_name");
                        imageUrl =
                            "http://graph.facebook.com/$userId/picture?type=large"

                        // TODO login by Facebook (API)
                        postLoginMedsos("facebook", tokenFB)

                        LoginManager.getInstance().logOut()
                    } catch (e: JSONException) {
                        e.printStackTrace()
                        return@newMeRequest
                    }
                }
                val parameters = Bundle()
                parameters.putString("fields", "id,name,email,gender") //,birthday");
                request.parameters = parameters
                request.executeAsync()
            }

            override fun onCancel() {
                // App code
                Log.v("LoginActivity", "cancel")
            }

            override fun onError(exception: FacebookException) {
                // App code
                Log.v("LoginActivity", exception.cause.toString())
            }
        })
    }

    override fun onResume() {
        super.onResume()
        val token = AccessToken.getCurrentAccessToken()
        if (token != null) {
//            Toast.makeText(this, token.getToken(), Toast.LENGTH_SHORT).show();
            tokenFB = token.token
            LoginManager.getInstance().logOut()
        }
    }

    override fun onActivityResult(
        requestCode: Int,
        resultCode: Int,
        data: Intent?
    ) {
        callbackManager!!.onActivityResult(requestCode, resultCode, data)
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 101) when (requestCode) {
            101 -> try {
                val task =
                    GoogleSignIn.getSignedInAccountFromIntent(data)
                val account =
                    task.getResult(ApiException::class.java)
                val nameGoogle = account!!.displayName
                val emailGoogle = account.email
                val token = account.idToken
                val getPhotoUrl = account.photoUrl.toString()
                googleSignInClient!!.signOut().addOnCompleteListener {
                    email = emailGoogle!!
                    name = nameGoogle!!
                    imageUrl = getPhotoUrl

//                    postLogin()
                    postLoginMedsos("google", token!!)
                }
            } catch (e: ApiException) {
                // The ApiException status code indicates the detailed failure reason.
                Log.w("TAG", "signInResult:failed code=" + e.statusCode)
            }
        }
    }

    private fun initKeyhash() {
        try {
            val info = packageManager.getPackageInfo(
                "com.catet.in",
                PackageManager.GET_SIGNATURES
            )
            for (signature in info.signatures) {
                val md = MessageDigest.getInstance("SHA")
                md.update(signature.toByteArray())
                val keyhash =
                    Base64.encodeToString(md.digest(), Base64.DEFAULT)
                Log.d("KeyHash:", keyhash)
            }
        } catch (ignored: PackageManager.NameNotFoundException) {
        } catch (ignored: NoSuchAlgorithmException) {
        }
    }

    /***** API Connection *****/
    private fun postLogin() {
        showProgressDialog()
        NetworkModule.create()
            .postLogin(GRANT_TYPE, CLIENT_ID, CLIENT_SECRET, "sheilaahana@gmail.com", "sheila")
            .subscribeOn(Schedulers.io())
//            .doOnTerminate { dismissProgressDialog() }
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ response ->
                Preferences.saveLogin(response)
                getUserProfile()
            }, { throwable ->
                dismissProgressDialog()
                ErrorHandler.handleErrorAPI(throwable, this)
            })
    }

    private fun postLoginMedsos(mediaType: String, mediaToken: String) {
        showProgressDialog()
        NetworkModule.create()
            .postLoginMedsos(GRANT_TYPE, CLIENT_ID, CLIENT_SECRET, mediaToken, mediaType, MyFirebaseMessagingService.token)
            .subscribeOn(Schedulers.io())
//            .doOnTerminate { dismissProgressDialog() }
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ response ->
                Preferences.saveLogin(response)
                getUserProfile()
            }, { throwable ->
                dismissProgressDialog()
                ErrorHandler.handleErrorAPI(throwable, this)
            })
    }

    private fun getUserProfile() {
        NetworkModule.create().getUserProfile()
            .subscribeOn(Schedulers.io())
            .doOnTerminate { dismissProgressDialog() }
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ response ->
                handleResponse(response.getData())
            }, { throwable ->
                ErrorHandler.handleErrorAPI(throwable, this)
            })
    }

    private fun handleResponse(data: Profile?) {
        data!!.image = imageUrl
        Preferences.saveProfile(data)

        startActivity(
            Intent(this@LoginAct, DashboardAct::class.java)
                .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
        )
    }
}
