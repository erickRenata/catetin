package com.catet.`in`.ui.home

import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContextCompat
import com.catet.`in`.R
import com.catet.`in`.base.BaseFragment
import com.catet.`in`.model.response.dashboard.Dashboard
import com.catet.`in`.network.ErrorHandler
import com.catet.`in`.network.NetworkModule
import com.catet.`in`.storage.Preferences
import com.catet.`in`.ui.dashboard.DashboardAct
import com.catet.`in`.ui.login.LoginAct
import com.catet.`in`.utils.Utils
import com.catet.`in`.utils.date.DateUtils
import com.squareup.picasso.Picasso
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_home.*


/**
 * Created by @erickrenata on 14/05/20.
 */

class HomeFragment : BaseFragment() {

    private lateinit var mContext: Context
    private lateinit var act: DashboardAct

    companion object {
        fun newInstance(): HomeFragment {
            return HomeFragment()
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        mContext = context
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view: View = inflater.inflate(R.layout.fragment_home, container, false)
        act = mContext as DashboardAct
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initView()
        initOnClickListener()
        getDetailDashboard()
        initSwipeRefresh()
    }

    private fun initSwipeRefresh() {
        pull_to_refresh.setProgressBackgroundColorSchemeColor(
            ContextCompat.getColor(
                mContext,
                R.color.colorWhite
            )
        )
        pull_to_refresh.setColorSchemeColors(ContextCompat.getColor(mContext, R.color.colorAccent))

        pull_to_refresh.setOnRefreshListener {
            pull_to_refresh.isRefreshing = false
            getDetailDashboard()
        }
    }

    private fun initOnClickListener() {
        btn_logout.setOnClickListener {
            showLogoutDialog()
        }
    }

    private fun initView() {
        val profile = Preferences.getProfile()
        Picasso.get().load(profile!!.image).into(iv_profile)
        tv_name.text = "Hi, " + profile.name
    }

    private fun showLogoutDialog() {
        val dialogClickListener =
            DialogInterface.OnClickListener { dialog: DialogInterface, which: Int ->
                when (which) {
                    DialogInterface.BUTTON_POSITIVE -> {
                        startActivity(
                            Intent(mContext, LoginAct::class.java)
                                .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
                        )
                        act.finish()

                        // TODO : remove all memory
                        Preferences.removeProfile()
                        Preferences.removeLogin()
                    }
                    DialogInterface.BUTTON_NEGATIVE -> dialog.dismiss()
                }
            }

        val builder =
            AlertDialog.Builder(mContext)
        builder.setMessage("Are you sure want to sign out?")
            .setPositiveButton("Yes", dialogClickListener)
            .setNegativeButton("No", dialogClickListener).show()
    }

    /***** API Connection *****/
    fun getDetailDashboard() {
        pull_to_refresh.isRefreshing = true
        NetworkModule.create().getDetailDashboard()
            .subscribeOn(Schedulers.io())
            .doOnTerminate { pull_to_refresh.isRefreshing = false }
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ response ->
                handleResponse(response.getData())
            }, { throwable ->
                ErrorHandler.handleErrorAPI(throwable, mContext)
            })
    }

    private fun handleResponse(data: Dashboard?) {
        // TODO : init value
        tv_total_hutang.text = Utils.convertNumberWithRp(data!!.summary.value.total_hutang)
        tv_total_piutang.text = Utils.convertNumberWithRp(data.summary.value.total_piutang.toInt())
        tv_total_hutang_terbesar.text =
            Utils.convertNumberWithRp(data.summary.value.terbesar_hutang)

        // TODO : init alert
        lin_alert.visibility = View.GONE
        if (data.summary.hutang.dead != null) {
            lin_alert.visibility = View.VISIBLE

            tv_alert.text = DateUtils.calculateRemainingDay(data.summary.hutang.dead.due_date) + " bayar hutang ke " + data.summary.hutang.dead.email_pemberi
        }
    }
}
