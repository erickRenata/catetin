package com.catet.`in`.ui.hutang

import android.Manifest
import android.app.Activity
import android.app.DatePickerDialog
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.text.*
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan
import android.text.style.ForegroundColorSpan
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.EditText
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.catet.`in`.R
import com.catet.`in`.base.BaseActivity
import com.catet.`in`.constant.Constant
import com.catet.`in`.network.ErrorHandler
import com.catet.`in`.network.NetworkModule
import com.catet.`in`.ui.image.ImagePreviewAct
import com.catet.`in`.utils.Utils
import com.catet.`in`.utils.date.DateUtils
import com.catet.`in`.utils.date.MyDatePickerDialog
import com.theartofdev.edmodo.cropper.CropImage
import com.theartofdev.edmodo.cropper.CropImageView
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_catat_hutang.*
import kotlinx.android.synthetic.main.activity_catat_hutang.iv_attachment
import kotlinx.android.synthetic.main.activity_catat_hutang.toolbar
import kotlinx.android.synthetic.main.activity_catat_hutang.tv_attachment
import kotlinx.android.synthetic.main.activity_detail_hutang.*
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.io.File
import java.text.DecimalFormat
import java.text.NumberFormat
import java.util.*

/**
 * Created by @erickrenata on 24/05/20.
 */

class CatatHutangAct : BaseActivity() {

    private var file: Uri? = null
    private var current = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_catat_hutang)
        initActionBar(toolbar, "Catat Hutang")

        initEdittext()
        initOnClickListener()
        initUploadPhotoText()
    }

    private fun initUploadPhotoText() {
        tv_attachment.text = "No Attachment"
        val text =
            "No Attachment (Upload photo)"
        val ss: Spannable = SpannableString(text)
        val clickableSpan1: ClickableSpan = object : ClickableSpan() {
            override fun onClick(widget: View) {
                ActivityCompat.requestPermissions(
                    this@CatatHutangAct, arrayOf(
                        Manifest.permission.CAMERA,
                        Manifest.permission.ACCESS_NETWORK_STATE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.READ_EXTERNAL_STORAGE
                    ),
                    Constant.PICK_FROM_CAMERA
                )
            }
        }
        ss.setSpan(
            clickableSpan1,
            text.length - 14,
            text.length,
            Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
        )
        ss.setSpan(
            ForegroundColorSpan(ContextCompat.getColor(this, R.color.colorAccent)),
            text.length - 14,
            text.length,
            Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
        )
        tv_attachment.text = ss
        tv_attachment.movementMethod = LinkMovementMethod.getInstance()
    }

    private fun initOnClickListener() {
        btn_show_date_start.setOnClickListener {
            hideAllSoftKeyboard(this)
            showDatePickerDialog(et_date_start)
        }
        btn_show_date_end.setOnClickListener {
            hideAllSoftKeyboard(this)
            showDatePickerDialog(et_date_end)
        }
        btn_save.setOnClickListener {
            if (validateForm()) {
                if (file == null) {
                    postCatatHutang()
                } else {
                    postCatatHutangWithPhoto(file)
                }
            }
        }
        iv_attachment.setOnClickListener {
            ActivityCompat.requestPermissions(
                this@CatatHutangAct, arrayOf(
                    Manifest.permission.CAMERA,
                    Manifest.permission.ACCESS_NETWORK_STATE,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    Manifest.permission.READ_EXTERNAL_STORAGE
                ),
                Constant.PICK_FROM_CAMERA
            )
        }
    }

    private fun initEdittext() {
        et_description.setOnEditorActionListener { _, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                hideSoftKeyboard(et_description)
                et_description.clearFocus()

                showDatePickerDialog(et_date_start)
                true
            } else false
        }

        et_amount.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(s: CharSequence?, p1: Int, p2: Int, p3: Int) {
                et_amount.removeTextChangedListener(this)

                try {
                    var originalString = s.toString().replace(".", ",")
                    val longval: Long
                    if (originalString.contains(",")) {
                        originalString = originalString.replace(",".toRegex(), "")
                    }
                    longval = originalString.toLong()
                    val formatter: DecimalFormat =
                        NumberFormat.getInstance(Locale.US) as DecimalFormat
                    formatter.applyPattern("#,###,###,###")
                    val formattedString: String = formatter.format(longval)

                    //setting text after format to EditText
                    et_amount.setText(formattedString.replace(",", "."))
                    et_amount.setSelection(et_amount.text.length)
                } catch (nfe: NumberFormatException) {
                    nfe.printStackTrace()
                }

                et_amount.addTextChangedListener(this)
            }
        })
    }

    private fun showDatePickerDialog(editText: EditText) {
        val c = Calendar.getInstance()
        val mYear: Int = c.get(Calendar.YEAR)
        val mMonth: Int = c.get(Calendar.MONTH)
        val mDay: Int = c.get(Calendar.DAY_OF_MONTH)
        val dpd = MyDatePickerDialog(
            this,
            DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                val montShow = monthOfYear + 1
                editText.setText(DateUtils.toSimpleDateFormat("$year-$montShow-$dayOfMonth"))

                if (editText == et_date_start) {
                    showDatePickerDialog(et_date_end)
                }
            },
            mYear,
            mMonth,
            mDay
        )
        if (editText == et_date_start) {
            dpd.datePicker.maxDate = Date().time
            showToastMessage("Pilih waktu hutang")
        } else {
            dpd.datePicker.minDate = Date().time
            showToastMessage("Pilih waktu peminjaman")
        }
        dpd.setPermanentTitle("HAI GANTENG")
        dpd.show()
    }

    private fun validateForm(): Boolean {
        if (et_email.text.toString().isEmpty()) {
            showToastMessage("Email tidak boleh kosong")
            return false
        }
        if (et_amount.text.toString().isEmpty()) {
            showToastMessage("Jumlah tidak boleh kosong")
            return false
        }
        if (et_location.text.toString().isEmpty()) {
            showToastMessage("Lokasi tidak boleh kosong")
            return false
        }
        if (et_description.text.toString().isEmpty()) {
            showToastMessage("Deskripsi tidak boleh kosong")
            return false
        }
        if (et_date_start.text.toString().isEmpty()) {
            showToastMessage("Waktu Hutang tidak boleh kosong")
            return false
        }
        if (et_date_end.text.toString().isEmpty()) {
            showToastMessage("Waktu Pengembalian tidak boleh kosong")
            return false
        }
        return true
    }

    private fun setPhotoImageWithUri(resultUri: Uri?) {
        file = resultUri
        iv_attachment.visibility = View.VISIBLE
        tv_attachment.visibility = View.GONE
        iv_attachment.setImageURI(resultUri)
    }

    /***** API Connection *****/
    private fun postCatatHutang() {
        showProgressDialog()
        NetworkModule.create()
            .postCatatHutang(
                et_email.text.toString(),
                DateUtils.parseDateToAPI(et_date_start.text.toString()),
                DateUtils.parseDateToAPI(et_date_end.text.toString()),
                et_description.text.toString(),
                et_location.text.toString(),
                et_amount.text.toString().replace(".", "")
            )
            .subscribeOn(Schedulers.io())
            .doOnTerminate { dismissProgressDialog() }
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ response ->
                showToastMessage("Hutang tersimpan")
                finish()
            }, { throwable ->
                dismissProgressDialog()
                ErrorHandler.handleErrorAPI(throwable, this)
            })
    }

    private fun postCatatHutangWithPhoto(uri: Uri?) {
        val file = File(Utils.getRealPathFromURI(uri!!, this))
        val reqFile: RequestBody = RequestBody.create("image/*".toMediaTypeOrNull(), file)
        val reqEmail: RequestBody =
            RequestBody.create("text/plain".toMediaTypeOrNull(), et_email.text.toString())
        val reqDate: RequestBody =
            RequestBody.create(
                "text/plain".toMediaTypeOrNull(),
                DateUtils.parseDateToAPI(et_date_start.text.toString())
            )
        val reqDueDate: RequestBody =
            RequestBody.create(
                "text/plain".toMediaTypeOrNull(),
                DateUtils.parseDateToAPI(et_date_end.text.toString())
            )
        val reqDescription: RequestBody =
            RequestBody.create("text/plain".toMediaTypeOrNull(), et_description.text.toString())
        val reqLocation: RequestBody =
            RequestBody.create("text/plain".toMediaTypeOrNull(), et_location.text.toString())
        val reqValue: RequestBody =
            RequestBody.create(
                "text/plain".toMediaTypeOrNull(),
                et_amount.text.toString().replace(".", "")
            )
        val image: MultipartBody.Part =
            MultipartBody.Part.createFormData("bukti_bayar", file.name, reqFile)

        showProgressDialog()
        NetworkModule.create().postUpdateCatatHutang(
            reqEmail,
            reqDate,
            reqDueDate,
            reqDescription,
            reqLocation,
            reqValue,
            image
        )
            .subscribeOn(Schedulers.io())
            .doOnTerminate { dismissProgressDialog() }
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                showToastMessage("Hutang tersimpan")
                finish()
            }, { throwable ->
                dismissProgressDialog()
                ErrorHandler.handleErrorAPI(throwable, this)
            })
    }

    /***** Result *****/
    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        when (requestCode) {
            Constant.PICK_FROM_CAMERA -> if (grantResults.isNotEmpty()
                && grantResults[0] == PackageManager.PERMISSION_GRANTED
            ) {
                CropImage.activity()
                    .setGuidelines(CropImageView.Guidelines.ON)
                    .start(this)
            } else {
                Toast.makeText(
                    this,
                    "Permission denied. Cannot open camera.",
                    Toast.LENGTH_SHORT
                ).show()
            }
            Constant.PICK_FROM_GALLERY -> if (grantResults.isNotEmpty()
                && grantResults[0] == PackageManager.PERMISSION_GRANTED
            ) {
                val pickPhoto = Intent(
                    Intent.ACTION_PICK,
                    MediaStore.Images.Media.EXTERNAL_CONTENT_URI
                )
                startActivityForResult(
                    pickPhoto,
                    Constant.PICK_FROM_GALLERY
                )
            } else {
                Toast.makeText(
                    this,
                    "Permission denied. Cannot choose an image.",
                    Toast.LENGTH_SHORT
                ).show()
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == RESULT_CANCELED) {
            // handle the cancellation
            return
        }

        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            val result = CropImage.getActivityResult(data)
            if (resultCode == Activity.RESULT_OK) {
                val resultUri = result.uri
                setPhotoImageWithUri(resultUri)
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                val error = result.error
                Toast.makeText(this, error.toString(), Toast.LENGTH_SHORT).show()
            }
        }
    }
}
