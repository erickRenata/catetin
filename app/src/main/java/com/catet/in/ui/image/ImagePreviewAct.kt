package com.catet.`in`.ui.image

import android.os.Bundle
import com.catet.`in`.R
import com.catet.`in`.base.BaseActivity
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_image_preview.*

/**
 * Created by @erickrenata on 10/06/20.
 */

class ImagePreviewAct : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_image_preview)

        val url = intent.getStringExtra("url")
        Picasso.get().load(url)
            .into(photo_view)
    }
}