package com.catet.`in`.ui.dashboard

import android.content.Intent
import android.os.Bundle
import android.text.SpannableStringBuilder
import android.text.Spanned
import android.view.View
import android.widget.TextView
import androidx.core.content.ContextCompat
import com.catet.`in`.R
import com.catet.`in`.base.BaseActivity
import com.catet.`in`.constant.Constant.Companion.HUTANG
import com.catet.`in`.constant.Constant.Companion.INTENT_NOTIFICATION_ID
import com.catet.`in`.constant.Constant.Companion.INTENT_NOTIFICATION_TYPE
import com.catet.`in`.ui.home.HomeFragment
import com.catet.`in`.ui.hutang.DetailHutangAct
import com.catet.`in`.ui.hutang.HutangFragment
import com.catet.`in`.ui.piutang.DetailPiutangAct
import com.catet.`in`.ui.piutang.PiutangFragment
import io.github.inflationx.calligraphy3.CalligraphyTypefaceSpan
import io.github.inflationx.calligraphy3.TypefaceUtils
import kotlinx.android.synthetic.main.activity_dashboard.*

/**
 * Created by @erickrenata on 14/05/20.
 */

class DashboardAct : BaseActivity() {

    private var homeFragment: HomeFragment = HomeFragment.newInstance()
    private var hutangFragment: HutangFragment = HutangFragment.newInstance()
    private var piutangFragment: PiutangFragment = PiutangFragment.newInstance()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dashboard)

        initMenuFragment()
        initMenuNavigationViewListener()

        initIntent()
    }

    private fun initIntent() {
        val id = intent.getStringExtra(INTENT_NOTIFICATION_ID)
        val type = intent.getStringExtra(INTENT_NOTIFICATION_TYPE)
        if (id != null && type != null) {
            if (type.toLowerCase() == HUTANG.toLowerCase()) {
                menuHutangSelected()
                startActivity(
                    Intent(this, DetailHutangAct::class.java)
                        .putExtra("id", id)
                )
            } else {
                menuPiutangSelected()
                startActivity(
                    Intent(this, DetailPiutangAct::class.java)
                        .putExtra("id", id)
                )
            }
        }
    }

    private fun initMenuFragment() {
        addFragment(R.id.fl_home, homeFragment)
        addFragment(R.id.fl_hutang, hutangFragment)
        addFragment(R.id.fl_piutang, piutangFragment)
    }

    private fun initMenuNavigationViewListener() {
        lin_home.setOnClickListener {
            menuDashboardSelected()
        }
        lin_hutang.setOnClickListener {
            menuHutangSelected()
        }
        lin_piutang.setOnClickListener {
            menuPiutangSelected()
        }
    }

    private fun menuDashboardSelected() {
        resetMenu()
        fl_home.visibility = View.VISIBLE
        tv_menu_home.setTextColor(ContextCompat.getColor(this, R.color.colorAccent))
        iv_menu_home.setImageResource(R.drawable.ic_menu_home_filled)
        updateFont(R.string.menu_home, tv_menu_home, 0)
        homeFragment.getDetailDashboard()
    }

    private fun menuHutangSelected() {
        resetMenu()
        fl_hutang.visibility = View.VISIBLE
        tv_menu_hutang.setTextColor(ContextCompat.getColor(this, R.color.colorAccent))
        iv_menu_hutang.setImageResource(R.drawable.ic_menu_hutang_filled)
        updateFont(R.string.menu_hutang, tv_menu_hutang, 0)
        hutangFragment.getHutangList()
    }

    private fun menuPiutangSelected() {
        resetMenu()
        fl_piutang.visibility = View.VISIBLE
        tv_menu_piutang.setTextColor(ContextCompat.getColor(this, R.color.colorAccent))
        iv_menu_piutang.setImageResource(R.drawable.ic_menu_piutang_filled)
        updateFont(R.string.menu_piutang, tv_menu_piutang, 0)
        piutangFragment.getPiutangList()
    }

    private fun resetMenu() {
        fl_home.visibility = View.GONE
        fl_hutang.visibility = View.GONE
        fl_piutang.visibility = View.GONE

        tv_menu_home.setTextColor(ContextCompat.getColor(this, R.color.colorLightGray))
        tv_menu_hutang.setTextColor(ContextCompat.getColor(this, R.color.colorLightGray))
        tv_menu_piutang.setTextColor(ContextCompat.getColor(this, R.color.colorLightGray))

        updateFont(R.string.menu_home, tv_menu_home, 1)
        updateFont(R.string.menu_hutang, tv_menu_hutang, 1)
        updateFont(R.string.menu_piutang, tv_menu_piutang, 1)

        iv_menu_home.setImageResource(R.drawable.ic_menu_home_gray)
        iv_menu_hutang.setImageResource(R.drawable.ic_menu_hutang_gray)
        iv_menu_piutang.setImageResource(R.drawable.ic_menu_piutang_gray)
    }

    private fun updateFont(message: Int, tv: TextView, type: Int) {
        val fontType = if (type == 1) "fonts/Gotham_Book.otf" else "fonts/Gotham_Medium.otf"
        val sBuilder = SpannableStringBuilder()
        sBuilder.append(getString(message))
        val typefaceSpan =
            CalligraphyTypefaceSpan(TypefaceUtils.load(assets, fontType))
        sBuilder.setSpan(
            typefaceSpan,
            0,
            getString(message).length,
            Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
        )
        tv.setText(sBuilder, TextView.BufferType.SPANNABLE)
    }
}