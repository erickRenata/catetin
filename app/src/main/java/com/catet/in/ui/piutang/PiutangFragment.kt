package com.catet.`in`.ui.piutang

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import com.catet.`in`.R
import com.catet.`in`.base.BaseFragment
import com.catet.`in`.model.response.piutang.PiutangData
import com.catet.`in`.network.ErrorHandler
import com.catet.`in`.network.NetworkModule
import com.catet.`in`.ui.dashboard.DashboardAct
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_piutang.*

/**
 * Created by @erickrenata on 14/05/20.
 */

class PiutangFragment : BaseFragment() {

    private lateinit var piutangAdapter: PiutangAdapter
    val list = ArrayList<PiutangData>()

    private lateinit var mContext: Context
    private lateinit var act: DashboardAct

    companion object {
        fun newInstance(): PiutangFragment {
            return PiutangFragment()
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        mContext = context
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view: View = inflater.inflate(R.layout.fragment_piutang, container, false)
        act = mContext as DashboardAct
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

//        getPiutangList()
        initSwipeRefresh()
        initOnClickListener()
    }

    override fun onResume() {
        super.onResume()
        getPiutangList()
    }

    private fun initOnClickListener() {
        btn_catat_piutang.setOnClickListener {
            startActivity(
                Intent(act, CatatPiutangAct::class.java)
            )
        }
    }

    private fun initSwipeRefresh() {
        pull_to_refresh.setProgressBackgroundColorSchemeColor(
            ContextCompat.getColor(
                mContext,
                R.color.colorWhite
            )
        )
        pull_to_refresh.setColorSchemeColors(ContextCompat.getColor(mContext, R.color.colorAccent))

        pull_to_refresh.setOnRefreshListener {
            pull_to_refresh.isRefreshing = false
            getPiutangList()
        }
    }

    private fun initAdapter() {
        piutangAdapter = PiutangAdapter(mContext, list)

        rv_content.setHasFixedSize(true)
        rv_content.layoutManager = LinearLayoutManager(mContext)
        rv_content.adapter = piutangAdapter
    }

    /***** API Connection *****/
    fun getPiutangList() {
        if (pull_to_refresh == null){
            return
        }
        pull_to_refresh.isRefreshing = true
        NetworkModule.create().getPiutangList()
            .subscribeOn(Schedulers.io())
            .doOnTerminate { pull_to_refresh.isRefreshing = false }
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ response ->
                handleResponse(response.getData())
            }, { throwable ->
                ErrorHandler.handleErrorAPI(throwable, mContext)
            })
    }

    private fun handleResponse(data: List<PiutangData>?) {
        list.clear()
        list.addAll(data!!)
        initAdapter()
    }
}
