package com.catet.`in`.ui.splash

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import com.catet.`in`.R
import com.catet.`in`.base.BaseActivity
import com.catet.`in`.storage.Preferences
import com.catet.`in`.ui.dashboard.DashboardAct
import com.catet.`in`.ui.login.LoginAct

/**
 * Created by @erickrenata on 11/05/20.
 */

class SplashAct : BaseActivity() {

    private val splashTime: Long = 1800 // 3 sec

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        Handler().postDelayed({
            if (Preferences.getLogin() != null) {
                startActivity(
                    Intent(this, DashboardAct::class.java)
                        .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
                )
            } else {
                startActivity(
                    Intent(this, LoginAct::class.java)
                        .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
                )
            }
            finish()
        }, splashTime)


    }
}
