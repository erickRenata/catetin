package com.catet.`in`.ui.piutang

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.text.Spannable
import android.text.SpannableString
import android.text.Spanned
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan
import android.text.style.ForegroundColorSpan
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.catet.`in`.R
import com.catet.`in`.base.BaseActivity
import com.catet.`in`.constant.Constant.Companion.LUNAS
import com.catet.`in`.constant.Constant.Companion.PICK_FROM_CAMERA
import com.catet.`in`.constant.Constant.Companion.PICK_FROM_GALLERY
import com.catet.`in`.model.response.piutang.PiutangData
import com.catet.`in`.network.ErrorHandler
import com.catet.`in`.network.NetworkModule
import com.catet.`in`.storage.Preferences
import com.catet.`in`.ui.image.ImagePreviewAct
import com.catet.`in`.utils.Utils
import com.catet.`in`.utils.date.DateUtils
import com.squareup.picasso.Picasso
import com.theartofdev.edmodo.cropper.CropImage
import com.theartofdev.edmodo.cropper.CropImageView
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_detail_piutang.*
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.io.File

/**
 * Created by @erickrenata on 24/05/20.
 */

class DetailPiutangAct : BaseActivity() {

    private var file: Uri? = null
    private var piutang: PiutangData? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_piutang)
        initActionBar(toolbar, "Detail Piutang")

        piutang = intent.getParcelableExtra(PiutangData::class.simpleName)
        if (piutang == null) {
            val id = intent.getStringExtra("id")
            getPiutangDetail(id)
        } else {
            initView()
        }
        initOnClickListener()
    }

    private fun initOnClickListener() {
        btn_set_lunas.setOnClickListener {
            postChangeStatus()
        }
        btn_reminder.setOnClickListener {
            postReminder()
        }
    }

    private fun initView() {
        tv_email.text = piutang!!.email_penerima
        tv_amount.text = Utils.convertNumberWithRp(piutang!!.value)
        tv_location.text = piutang!!.location
        tv_description.text = piutang!!.description
        tv_date_start.text = DateUtils.toSimpleDateFormat(piutang!!.date)
        tv_date_end.text = DateUtils.toSimpleDateFormat(piutang!!.due_date)
        tv_status.text = piutang!!.status
        btn_reminder.visibility = View.GONE
        if (piutang!!.status == LUNAS) {
            tv_status.setTextColor(ContextCompat.getColor(this, R.color.colorGreen))
        } else {
            btn_reminder.visibility = View.VISIBLE
            tv_status.setTextColor(ContextCompat.getColor(this, R.color.colorRed))
        }

        btn_set_lunas.visibility = View.GONE
        val profile = Preferences.getProfile()
        if (piutang!!.input_by == profile!!.id) {
            if (piutang!!.status != LUNAS) {
                btn_set_lunas.visibility = View.VISIBLE
            }
        }

        if (piutang!!.bukti_transfer != null) {
            iv_attachment.visibility = View.VISIBLE
            tv_attachment.visibility = View.GONE
            if (piutang!!.bukti_transfer!!.isNotEmpty()) {
                Picasso.get().load(piutang!!.bukti_transfer)
                    .into(iv_attachment)
                iv_attachment.setOnClickListener {
                    startActivity(
                        Intent(this, ImagePreviewAct::class.java)
                            .putExtra("url", piutang!!.bukti_transfer)
                    )
                }
            }
        } else {
            iv_attachment.visibility = View.GONE
            tv_attachment.visibility = View.VISIBLE

            tv_attachment.text = "No Attachment"
            if (piutang!!.editable) {
                if (piutang!!.status != LUNAS) {
                    val text =
                        "No Attachment (Upload photo)"
                    val ss: Spannable = SpannableString(text)
                    val clickableSpan1: ClickableSpan = object : ClickableSpan() {
                        override fun onClick(widget: View) {
                            ActivityCompat.requestPermissions(
                                this@DetailPiutangAct, arrayOf(
                                    Manifest.permission.CAMERA,
                                    Manifest.permission.ACCESS_NETWORK_STATE,
                                    Manifest.permission.WRITE_EXTERNAL_STORAGE,
                                    Manifest.permission.READ_EXTERNAL_STORAGE
                                ),
                                PICK_FROM_CAMERA
                            )
                        }
                    }
                    ss.setSpan(
                        clickableSpan1,
                        text.length - 14,
                        text.length,
                        Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
                    )
                    ss.setSpan(
                        ForegroundColorSpan(ContextCompat.getColor(this, R.color.colorAccent)),
                        text.length - 14,
                        text.length,
                        Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
                    )
                    tv_attachment.text = ss
                    tv_attachment.movementMethod = LinkMovementMethod.getInstance()
                }
            }

        }
    }

    private fun showChooseImageDialog() {
        val choices =
            arrayOf("From Camera", "From Gallery")
        val builder =
            AlertDialog.Builder(this)
        builder.setTitle("Change Photo")
        builder.setItems(choices) { dialog, which -> // the user clicked on colors[which]
            if (which == 0) {
                ActivityCompat.requestPermissions(
                    this, arrayOf(
                        Manifest.permission.CAMERA,
                        Manifest.permission.ACCESS_NETWORK_STATE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.READ_EXTERNAL_STORAGE
                    ),
                    PICK_FROM_CAMERA
                )
            } else {
                ActivityCompat.requestPermissions(
                    this, arrayOf(
                        Manifest.permission.CAMERA,
                        Manifest.permission.ACCESS_NETWORK_STATE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.READ_EXTERNAL_STORAGE
                    ),
                    PICK_FROM_GALLERY
                )
            }
            dialog.dismiss()
        }
        builder.show()
    }

    private fun setPhotoImageWithUri(resultUri: Uri?) {
        file = resultUri
        iv_attachment.visibility = View.VISIBLE
        tv_attachment.visibility = View.GONE
        iv_attachment.setImageURI(resultUri)
        postUploadPhotoPiutang(file)
    }

    /***** API Connection *****/
    private fun postChangeStatus() {
        showProgressDialog()
        NetworkModule.create()
            .postChangeStatus(
                piutang!!.id.toString(),
                LUNAS
            )
            .subscribeOn(Schedulers.io())
//            .doOnTerminate { dismissProgressDialog() }
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ response ->
                getPiutangDetail(piutang!!.id.toString())
            }, { throwable ->
                dismissProgressDialog()
                ErrorHandler.handleErrorAPI(throwable, this)
            })
    }

    private fun getPiutangDetail(id: String) {
        NetworkModule.create().getPiutangDetail(id)
            .subscribeOn(Schedulers.io())
            .doOnTerminate { dismissProgressDialog() }
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ response ->
                piutang = response.getData()
                initView()
            }, { throwable ->
                ErrorHandler.handleErrorAPI(throwable, this)
            })
    }

    private fun postReminder() {
        showProgressDialog()
        NetworkModule.create()
            .getNotifyPayDebt(
                piutang!!.id.toString()
            )
            .subscribeOn(Schedulers.io())
            .doOnTerminate { dismissProgressDialog() }
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ response ->
                showToastMessage("Ingatkan berhasil")
            }, { throwable ->
                dismissProgressDialog()
                ErrorHandler.handleErrorAPI(throwable, this)
            })
    }

    private fun postUploadPhotoPiutang(uri: Uri?) {
        val file = File(Utils.getRealPathFromURI(uri!!, this))
        val reqFile: RequestBody = RequestBody.create("image/*".toMediaTypeOrNull(), file)
        val reqEmail: RequestBody =
            RequestBody.create("text/plain".toMediaTypeOrNull(), piutang!!.email_penerima)
        val reqDate: RequestBody =
            RequestBody.create("text/plain".toMediaTypeOrNull(), piutang!!.date)
        val reqDueDate: RequestBody =
            RequestBody.create("text/plain".toMediaTypeOrNull(), piutang!!.due_date)
        val reqDescription: RequestBody =
            RequestBody.create("text/plain".toMediaTypeOrNull(), piutang!!.description)
        val reqLocation: RequestBody =
            RequestBody.create("text/plain".toMediaTypeOrNull(), piutang!!.location)
        val reqValue: RequestBody =
            RequestBody.create("text/plain".toMediaTypeOrNull(), piutang!!.value.toString())
        val reqId: RequestBody =
            RequestBody.create("text/plain".toMediaTypeOrNull(), piutang!!.id.toString())
        val image: MultipartBody.Part =
            MultipartBody.Part.createFormData("bukti_transfer", file.name, reqFile)

        showProgressDialog()
        NetworkModule.create().postUpdateCatatPiutang(
            reqEmail,
            reqDate,
            reqDueDate,
            reqDescription,
            reqLocation,
            reqValue,
            image,
            reqId
        )
            .subscribeOn(Schedulers.io())
//            .doOnTerminate { dismissProgressDialog() }
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                getPiutangDetail(piutang!!.id.toString())
            }, { throwable ->
                dismissProgressDialog()
                ErrorHandler.handleErrorAPI(throwable, this)
            })
    }

    /***** Result *****/
    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        when (requestCode) {
            PICK_FROM_CAMERA -> if (grantResults.isNotEmpty()
                && grantResults[0] == PackageManager.PERMISSION_GRANTED
            ) {
                CropImage.activity()
                    .setGuidelines(CropImageView.Guidelines.ON)
                    .start(this)
            } else {
                Toast.makeText(
                    this,
                    "Permission denied. Cannot open camera.",
                    Toast.LENGTH_SHORT
                ).show()
            }
            PICK_FROM_GALLERY -> if (grantResults.isNotEmpty()
                && grantResults[0] == PackageManager.PERMISSION_GRANTED
            ) {
                val pickPhoto = Intent(
                    Intent.ACTION_PICK,
                    MediaStore.Images.Media.EXTERNAL_CONTENT_URI
                )
                startActivityForResult(
                    pickPhoto,
                    PICK_FROM_GALLERY
                )
            } else {
                Toast.makeText(
                    this,
                    "Permission denied. Cannot choose an image.",
                    Toast.LENGTH_SHORT
                ).show()
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == RESULT_CANCELED) {
            // handle the cancellation
            return
        }

        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            val result = CropImage.getActivityResult(data)
            if (resultCode == Activity.RESULT_OK) {
                val resultUri = result.uri
                setPhotoImageWithUri(resultUri)
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                val error = result.error
                Toast.makeText(this, error.toString(), Toast.LENGTH_SHORT).show()
            }
        }
    }
}