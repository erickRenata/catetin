package com.catet.`in`.ui.hutang

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.catet.`in`.R
import com.catet.`in`.constant.Constant
import com.catet.`in`.constant.Constant.Companion.LUNAS
import com.catet.`in`.model.response.hutang.HutangData
import com.catet.`in`.utils.date.DateUtils
import kotlinx.android.synthetic.main.item_hutang.view.*

/**
 * Created by @erickrenata on 24/05/20.
 */

class HutangAdapter(
    private val context: Context,
    private val list: ArrayList<HutangData>
) :
    RecyclerView.Adapter<HutangAdapter.Holder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
        return Holder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.item_hutang,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int = list.size

    override fun onBindViewHolder(holder: Holder, position: Int) {
        holder.view.tv_name.text = list[position].email_pemberi
        holder.view.tv_date.text = DateUtils.calculateRemainingDay(list[position].due_date)
        holder.view.tv_status.text = list[position].status

        if (list[position].status == LUNAS) {
            holder.view.tv_status.setTextColor(ContextCompat.getColor(context, R.color.colorGreen))
        } else {
            holder.view.tv_status.setTextColor(ContextCompat.getColor(context, R.color.colorRed))
        }
        holder.view.setOnClickListener {
            context.startActivity(
                Intent(context, DetailHutangAct::class.java)
                    .putExtra(HutangData::class.simpleName, list[position])
            )
        }
    }

    class Holder(val view: View) : RecyclerView.ViewHolder(view)

}