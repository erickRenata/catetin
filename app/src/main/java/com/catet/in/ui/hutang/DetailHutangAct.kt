package com.catet.`in`.ui.hutang

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.text.Spannable
import android.text.SpannableString
import android.text.Spanned
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan
import android.text.style.ForegroundColorSpan
import android.view.View
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.catet.`in`.R
import com.catet.`in`.base.BaseActivity
import com.catet.`in`.constant.Constant
import com.catet.`in`.model.response.hutang.HutangData
import com.catet.`in`.network.ErrorHandler
import com.catet.`in`.network.NetworkModule
import com.catet.`in`.storage.Preferences
import com.catet.`in`.ui.image.ImagePreviewAct
import com.catet.`in`.utils.Utils
import com.catet.`in`.utils.date.DateUtils
import com.squareup.picasso.Picasso
import com.theartofdev.edmodo.cropper.CropImage
import com.theartofdev.edmodo.cropper.CropImageView
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_detail_hutang.*
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.io.File

/**
 * Created by @erickrenata on 24/05/20.
 */

class DetailHutangAct : BaseActivity() {

    private var file: Uri? = null
    private var hutang: HutangData? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_hutang)
        initActionBar(toolbar, "Detail Hutang")

        hutang = intent.getParcelableExtra(HutangData::class.simpleName)
        if (hutang == null) {
            val id = intent.getStringExtra("id")
            getHutangDetail(id)
        } else {
            initView()
        }
        initOnClickListener()
    }

    private fun initOnClickListener() {
        btn_set_lunas.setOnClickListener {
            postChangeStatus()
        }
        btn_reminder.setOnClickListener {
            postReminder()
        }
    }

    private fun initView() {
        tv_email.text = hutang!!.email_pemberi
        tv_amount.text = Utils.convertNumberWithRp(hutang!!.value)
        tv_location.text = hutang!!.location
        tv_description.text = hutang!!.description
        tv_date_start.text = DateUtils.toSimpleDateFormat(hutang!!.date)
        tv_date_end.text = DateUtils.toSimpleDateFormat(hutang!!.due_date)
        tv_status.text = hutang!!.status
        if (hutang!!.status == Constant.LUNAS) {
            tv_status.setTextColor(ContextCompat.getColor(this, R.color.colorGreen))
        } else {
            tv_status.setTextColor(ContextCompat.getColor(this, R.color.colorRed))
        }

        btn_set_lunas.visibility = View.GONE
        btn_reminder.visibility = View.GONE
        val profile = Preferences.getProfile()
        if (hutang!!.input_by == profile!!.id) {
            if (hutang!!.status != Constant.LUNAS) {
                btn_set_lunas.visibility = View.VISIBLE
            }
        } else {
            if (hutang!!.status != Constant.LUNAS) {
                btn_reminder.visibility = View.VISIBLE
            }
        }

        if (hutang!!.bukti_bayar != null) {
            iv_attachment.visibility = View.VISIBLE
            tv_attachment.visibility = View.GONE
            if (hutang!!.bukti_bayar!!.isNotEmpty()) {
                Picasso.get().load(hutang!!.bukti_bayar)
                    .into(iv_attachment)
                iv_attachment.setOnClickListener {
                    startActivity(
                        Intent(this, ImagePreviewAct::class.java)
                            .putExtra("url", hutang!!.bukti_bayar)
                    )
                }
            }
        } else {
            iv_attachment.visibility = View.GONE
            tv_attachment.visibility = View.VISIBLE

            tv_attachment.text = "No Attachment"
            if (hutang!!.editable) {
                if (hutang!!.status != Constant.LUNAS) {
                    val text =
                        "No Attachment (Upload photo)"
                    val ss: Spannable = SpannableString(text)
                    val clickableSpan1: ClickableSpan = object : ClickableSpan() {
                        override fun onClick(widget: View) {
                            ActivityCompat.requestPermissions(
                                this@DetailHutangAct, arrayOf(
                                    Manifest.permission.CAMERA,
                                    Manifest.permission.ACCESS_NETWORK_STATE,
                                    Manifest.permission.WRITE_EXTERNAL_STORAGE,
                                    Manifest.permission.READ_EXTERNAL_STORAGE
                                ),
                                Constant.PICK_FROM_CAMERA
                            )
                        }
                    }
                    ss.setSpan(
                        clickableSpan1,
                        text.length - 14,
                        text.length,
                        Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
                    )
                    ss.setSpan(
                        ForegroundColorSpan(ContextCompat.getColor(this, R.color.colorAccent)),
                        text.length - 14,
                        text.length,
                        Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
                    )
                    tv_attachment.text = ss
                    tv_attachment.movementMethod = LinkMovementMethod.getInstance()
                }
            }

        }
    }

    private fun setPhotoImageWithUri(resultUri: Uri?) {
        file = resultUri
        iv_attachment.visibility = View.VISIBLE
        tv_attachment.visibility = View.GONE
        iv_attachment.setImageURI(resultUri)
        postUploadPhotoPiutang(file)
    }

    /***** API Connection *****/
    private fun postChangeStatus() {
        showProgressDialog()
        NetworkModule.create()
            .postChangeStatus(
                hutang!!.id.toString(),
                Constant.LUNAS
            )
            .subscribeOn(Schedulers.io())
//            .doOnTerminate { dismissProgressDialog() }
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ response ->
                getHutangDetail(hutang!!.id.toString())
            }, { throwable ->
                dismissProgressDialog()
                ErrorHandler.handleErrorAPI(throwable, this)
            })
    }

    fun getHutangDetail(id: String) {
        NetworkModule.create().getHutangDetail(id)
            .subscribeOn(Schedulers.io())
            .doOnTerminate { dismissProgressDialog() }
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ response ->
                hutang = response.getData()
                initView()
            }, { throwable ->
                ErrorHandler.handleErrorAPI(throwable, this)
            })
    }

    private fun postReminder() {
        showProgressDialog()
        NetworkModule.create()
            .getNotifyDebtPaid(
                hutang!!.id.toString()
            )
            .subscribeOn(Schedulers.io())
            .doOnTerminate { dismissProgressDialog() }
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ response ->
                showToastMessage("Ingatkan berhasil")
            }, { throwable ->
                dismissProgressDialog()
                ErrorHandler.handleErrorAPI(throwable, this)
            })
    }

    private fun postUploadPhotoPiutang(uri: Uri?) {
        val file = File(Utils.getRealPathFromURI(uri!!, this))
        val reqFile: RequestBody = RequestBody.create("image/*".toMediaTypeOrNull(), file)
        val reqEmail: RequestBody =
            RequestBody.create("text/plain".toMediaTypeOrNull(), hutang!!.email_pemberi)
        val reqDate: RequestBody =
            RequestBody.create("text/plain".toMediaTypeOrNull(), hutang!!.date)
        val reqDueDate: RequestBody =
            RequestBody.create("text/plain".toMediaTypeOrNull(), hutang!!.due_date)
        val reqDescription: RequestBody =
            RequestBody.create("text/plain".toMediaTypeOrNull(), hutang!!.description)
        val reqLocation: RequestBody =
            RequestBody.create("text/plain".toMediaTypeOrNull(), hutang!!.location)
        val reqValue: RequestBody =
            RequestBody.create("text/plain".toMediaTypeOrNull(), hutang!!.value.toString())
        val reqId: RequestBody =
            RequestBody.create("text/plain".toMediaTypeOrNull(), hutang!!.id.toString())
        val image: MultipartBody.Part =
            MultipartBody.Part.createFormData("bukti_bayar", file.name, reqFile)

        showProgressDialog()
        NetworkModule.create().postUpdateCatatHutang(
            reqEmail,
            reqDate,
            reqDueDate,
            reqDescription,
            reqLocation,
            reqValue,
            image,
            reqId
        )
            .subscribeOn(Schedulers.io())
//            .doOnTerminate { dismissProgressDialog() }
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                getHutangDetail(hutang!!.id.toString())
            }, { throwable ->
                dismissProgressDialog()
                ErrorHandler.handleErrorAPI(throwable, this)
            })
    }

    /***** Result *****/
    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        when (requestCode) {
            Constant.PICK_FROM_CAMERA -> if (grantResults.isNotEmpty()
                && grantResults[0] == PackageManager.PERMISSION_GRANTED
            ) {
                CropImage.activity()
                    .setGuidelines(CropImageView.Guidelines.ON)
                    .start(this)
            } else {
                Toast.makeText(
                    this,
                    "Permission denied. Cannot open camera.",
                    Toast.LENGTH_SHORT
                ).show()
            }
            Constant.PICK_FROM_GALLERY -> if (grantResults.isNotEmpty()
                && grantResults[0] == PackageManager.PERMISSION_GRANTED
            ) {
                val pickPhoto = Intent(
                    Intent.ACTION_PICK,
                    MediaStore.Images.Media.EXTERNAL_CONTENT_URI
                )
                startActivityForResult(
                    pickPhoto,
                    Constant.PICK_FROM_GALLERY
                )
            } else {
                Toast.makeText(
                    this,
                    "Permission denied. Cannot choose an image.",
                    Toast.LENGTH_SHORT
                ).show()
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == RESULT_CANCELED) {
            // handle the cancellation
            return
        }

        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            val result = CropImage.getActivityResult(data)
            if (resultCode == Activity.RESULT_OK) {
                val resultUri = result.uri
                setPhotoImageWithUri(resultUri)
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                val error = result.error
                Toast.makeText(this, error.toString(), Toast.LENGTH_SHORT).show()
            }
        }
    }
}