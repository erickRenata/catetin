package com.catet.`in`.base

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.LayoutInflater
import android.view.Window
import android.view.WindowManager
import androidx.fragment.app.DialogFragment

/**
 * Created by @erickrenata on 11/05/20.
 */

@SuppressLint("Registered")
open class BaseDialogFragment : DialogFragment() {
    protected var mContext: Context? = null
    protected var mLayoutInflater: LayoutInflater? = null

    //    private ProgressDialog mProgressDialog;

    override fun onAttach(context: Context) {
        super.onAttach(context)
        mContext = activity
        mLayoutInflater = LayoutInflater.from(mContext)
    }

    protected fun getDefaultDialog(): Dialog {
        val dialog = Dialog(mContext!!)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.window!!.setLayout(
            WindowManager.LayoutParams.MATCH_PARENT,
            WindowManager.LayoutParams.MATCH_PARENT
        )
        return dialog
    }

    fun getBaseActivity(): BaseActivity? {
        return activity as BaseActivity?
    }

    protected fun showProgressDialog() {
//        mProgressDialog = ProgressDialog.create();
//        mProgressDialog.show(getFragmentManager());
    }

    protected fun dismissProgressDialog() {
//        mProgressDialog.dismiss();
    }
}

