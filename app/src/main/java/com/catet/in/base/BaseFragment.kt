package com.catet.`in`.base

import android.app.ProgressDialog
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager

/**
 * Created by @erickrenata on 11/05/20.
 */

abstract class BaseFragment : Fragment(){

    lateinit var mProgressDialog: ProgressDialog

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        mProgressDialog = ProgressDialog(activity)
    }

    fun showLoading(){
        mProgressDialog.setCancelable(false)
        mProgressDialog.show()
    }

    fun showLoading(message: String){
        mProgressDialog.setMessage(message)
        mProgressDialog.setCancelable(false)
        mProgressDialog.show()
    }

    fun dismissLoading(){
        if (mProgressDialog.isShowing){
            mProgressDialog.dismiss()
        }
    }

    protected fun addFragment(frameContainer: Int, fragment: Fragment) {
        for (i in 0..childFragmentManager.backStackEntryCount) {
            childFragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE)
        }
        childFragmentManager
            .beginTransaction()
//            .setCustomAnimations(R.anim.design_bottom_sheet_slide_in, R.anim.design_bottom_sheet_slide_out)
            .replace(frameContainer, fragment, fragment.javaClass.simpleName)
            .commit()
    }

}
