package com.catet.`in`.storage

import com.catet.`in`.model.response.login.Login
import com.catet.`in`.model.response.profile.Profile
import com.orhanobut.hawk.Hawk


/**
 * Created by @erickrenata on 11/05/20.
 */

class Preferences {

    object Key {
        const val LOGIN = "Key.Login"
        const val PROFILE = "Key.Profile"
    }

    companion object {
        fun saveLogin(profile: Login?) {
            Hawk.put(Key.LOGIN, profile)
        }

        fun getLogin(): Login? {
            return Hawk.get(Key.LOGIN)
        }

        fun removeLogin() {
            Hawk.delete(Key.LOGIN)
        }

        fun saveProfile(profile: Profile?) {
            Hawk.put(Key.PROFILE, profile)
        }

        fun getProfile(): Profile? {
            return Hawk.get(Key.PROFILE)
        }

        fun removeProfile() {
            Hawk.delete(Key.PROFILE)
        }
    }
}