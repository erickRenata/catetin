package com.catet.`in`.model.response.hutang

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class HutangData(
    val created_at: String,
    val date: String,
    val day_left: String,
    val description: String,
    val due_date: String,
    val editable: Boolean,
    val email_pemberi: String,
    val email_penerima: String,
    val id: Int,
    val value: Int,
    val input_by: String,
    val location: String,
    val bukti_bayar: String?,
    val bukti_transfer: String?,
    val paid_date: String?,
    val status: String,
    val updated_at: String
) : Parcelable