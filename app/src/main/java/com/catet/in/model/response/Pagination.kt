package com.catet.`in`.model.response

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

/**
 * Created by @erickrenata on 11/05/20.
 */

@Parcelize
data class Pagination(
    val current_page: Int = 0,
    val first_page_url: String = "",
    val from: Int = 0,
    val last_page: Int = 0,
    val last_page_url: String = "",
    val next_page_url: String = "",
    val path: String = "",
    val per_page: Int = 0,
    val prev_page_url: String = "",
    val to: Int = 0,
    val total: Int = 0
) : Parcelable