package com.catet.`in`.model.response.dashboard

import com.catet.`in`.model.response.piutang.PiutangData

data class Piutang(
    val belum_lunas: Int,
    val dead: PiutangData,
    val deadline: List<PiutangData>,
    val lunas: Int,
    val total: Int
)