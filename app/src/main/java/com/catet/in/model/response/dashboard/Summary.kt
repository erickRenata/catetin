package com.catet.`in`.model.response.dashboard

data class Summary(
    val hutang: Hutang,
    val piutang: Piutang,
    val total_transaksi: Int,
    val value: Value
)