package com.catet.`in`.model.error

/**
 * Created by @erickrenata on 11/05/20.
 */

data class Errors(
    val error: String,
    val error_description: String,
    val message: String
)