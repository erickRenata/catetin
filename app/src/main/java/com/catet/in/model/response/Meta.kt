package com.catet.`in`.model.response

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize


/**
 * Created by @erickrenata on 11/05/20.
 */

@Parcelize
data class Meta(
    val code: Int = 0,
    val message: String = "",
    val message_array: List<String> = mutableListOf(),
    val response_date: String = "",
    val response_time: Double = 0.0
) : Parcelable