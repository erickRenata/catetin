package com.catet.`in`.model.response.dashboard

data class Value(
    val hutang: Int,
    val piutang: String,
    val terbesar_hutang: Int,
    val terbesar_piutang: Int,
    val total_hutang: Int,
    val total_piutang: String
)