package com.catet.`in`.model.error


/**
 * Created by @erickrenata on 11/05/20.
 */

data class ErrorModel(
    val meta: Errors
)