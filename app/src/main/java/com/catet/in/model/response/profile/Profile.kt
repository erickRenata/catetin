package com.catet.`in`.model.response.profile

data class Profile(
    val id: String?,
    val name: String?,
    val email: String?,
    val email_verified_at: String?,
    val provider_id: String?,
    val created_at: String?,
    val updated_at: String?,
    var image: String?
)