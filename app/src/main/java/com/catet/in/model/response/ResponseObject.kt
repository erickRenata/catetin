package com.catet.`in`.model.response

import com.catet.`in`.base.BaseResponse

/**
 * Created by @erickrenata on 11/05/20.
 */

class ResponseObject<T> : BaseResponse() {
    private val data: T? = null
    fun getData(): T? {
        return data
    }
}