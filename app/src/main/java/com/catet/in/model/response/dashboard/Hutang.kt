package com.catet.`in`.model.response.dashboard

import com.catet.`in`.model.response.hutang.HutangData

data class Hutang(
    val belum_lunas: Int,
    val dead: HutangData,
    val deadline: List<HutangData>,
    val lunas: Int,
    val total: Int
)