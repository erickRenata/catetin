package com.catet.`in`.model.response.login

/**
 * Created by @erickrenata on 28/05/20.
 */

data class Login(
    val access_token: String,
    val expires_in: Int,
    val refresh_token: String,
    val token_type: String
)