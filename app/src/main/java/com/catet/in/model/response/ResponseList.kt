package com.catet.`in`.model.response

import com.catet.`in`.base.BaseResponse


/**
 * Created by @erickrenata on 11/05/20.
 */

class ResponseList<T> : BaseResponse() {
    private val data: List<T>? = null
    fun getData(): List<T>? {
        return data
    }
}